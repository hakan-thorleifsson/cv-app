//
//  ExperienceTableViewController.swift
//  hakanthorleifssonCVapp
//
//  Created by Håkan Thorleifsson on 2019-10-31.
//  Copyright © 2019 Håkan Thorleifsson. All rights reserved.
//

import UIKit

class ExperienceTableViewController: UITableViewController {
    
    struct Experience {
        var title : String
        var text : String
        var image : String
        var description: String
    }
    
    var experienceObjects = [
        [Experience(title: "Papaja Developer", text: "2021-2025", image: "Papaja", description: getDesc(n:0)),
         Experience(title: "Game Developer", text: "2025-2028", image: "Game", description: getDesc(n:1)),
         Experience(title: "Self-Driving Car Developer", text: "2028-", image: "Car", description: getDesc(n:2))],

        [Experience(title: "Jönköping University", text: "2018-2021", image: "School", description: getDesc(n:3))],
    ]


    let sectionTitles = ["Work", "Education"]
     
    override func numberOfSections(in tableView: UITableView) -> Int {
         return sectionTitles.count
        }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experienceObjects[section].count
     }
     
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         return sectionTitles[section]
     }

    override func tableView(_ tableView: UITableView,cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell? {

        let cellObjects = experienceObjects[indexPath.section][indexPath.row]
        cell.textLabel?.text = cellObjects.title
        cell.detailTextLabel?.text = cellObjects.text
        cell.imageView?.image = UIImage(named: cellObjects.image)
        
        return cell
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let segueIndex = experienceObjects[indexPath.section][indexPath.item]
        performSegue(withIdentifier: "segue", sender: segueIndex)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? ExperienceDetailViewController
        let destSender: Experience? = sender as? Experience
        destination?.theTitle = destSender?.title
        destination?.theText = destSender?.text
        destination?.theImage = UIImage(named: destSender?.image ?? "" )
        destination?.theDesc = destSender?.description
        
    }

}


func getDesc(n: Int)->String{
    if n == 0{
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in purus lobortis, gravida urna eu, fermentum urna. Vivamus non tincidunt augue. Morbi et iaculis elit, vel luctus turpis. Phasellus ultricies mauris et felis cursus faucibus. Sed iaculis diam vitae sapien mollis ullamcorper. Nullam viverra id metus sit amet pellentesque. Donec tincidunt ullamcorper laoreet. Sed eget ante eget erat porta mattis. Morbi sodales velit sit amet nibh auctor, eget pretium urna ultrices. Praesent at ultrices magna. Sed vel maximus lacus. Sed placerat turpis non lectus cursus lobortis. Integer vel dolor fermentum, mollis nibh et, lobortis sapien. Aliquam erat volutpat. Duis tempus velit ultricies mollis posuere. Vivamus scelerisque sodales lacus a lobortis."
    }
    else if n == 1{
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in purus lobortis, gravida urna eu, fermentum urna. Vivamus non tincidunt augue. Morbi et iaculis elit, vel luctus turpis. Phasellus ultricies mauris et felis cursus faucibus. Sed iaculis diam vitae sapien mollis ullamcorper. Nullam viverra id metus sit amet pellentesque. Donec tincidunt ullamcorper laoreet. Sed eget ante eget erat porta mattis. Morbi sodales velit sit amet nibh auctor, eget pretium urna ultrices. Praesent at ultrices magna. Sed vel maximus lacus. Sed placerat turpis non lectus cursus lobortis. Integer vel dolor fermentum, mollis nibh et, lobortis sapien. Aliquam erat volutpat. Duis tempus velit ultricies mollis posuere. Vivamus scelerisque sodales lacus a lobortis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in purus lobo"
    }
    else if n == 2{
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in purus lobortis, gravida urna eu, fermentum urna. Vivamus non tincidunt augue. Morbi et iaculis elit, vel luctus turpis. Phasellus ultricies mauris et felis cursus faucibus. Sed iaculis diam vitae sapien mollis ullamcorper. Nullam viverra id metus sit amet pellentesque. Donec tincidunt ullamcorper laoreet. Sed eget ante eget erat porta mattis. Morbi sodales velit sit amet nibh auctor, eget pretium urna ultrices. Praesent at ultrices magna. Sed vel maximus lacus. Sed placerat turpis non lectus cursus lobortis. Integer vel dolor fermentum, mollis nibh et, lobortis sapien. Aliquam erat volutpat. Duis tempus velit ultricies mollis posuere. Vivamus scelerisque sodales lacus a lobortis."
    }
    else {
        return " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in purus lobortis, gravida urna eu, fermentum urna. Vivamus non tincidunt augue. Morbi et iaculis elit, vel luctus turpis. Phasellus ultricies mauris et felis cursus faucibus. Sed iaculis diam vitae sapien mollis ullamcorper. Nullam viverra id metus sit amet pellentesque. Donec tincidunt ullamcorper laoreet. Sed eget ante eget erat porta mattis. Morbi sodales velit sit amet nibh auctor, eget pretium urna ultrices. Praesent at ultrices magna. Sed vel maximus lacus. Sed placerat turpis non lectus cursus lobortis. Integer vel dolor fermentum, mollis nibh et, lobortis sapien. Aliquam erat volutpat. Duis tempus velit ultricies mollis posuere. Vivamus scelerisque sodales lacus a lobortis."
    }
}
