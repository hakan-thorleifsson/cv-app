//
//  ExperienceDetailViewController.swift
//  hakanthorleifssonCVapp
//
//  Created by Håkan Thorleifsson on 2019-11-02.
//  Copyright © 2019 Håkan Thorleifsson. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var labelDetailDesc: UILabel!
    
    var theTitle: String?
    var theText: String?
    var theImage: UIImage?
    var theDesc: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }
    
    func setUI(){
        labelTitle.text = theTitle
        labelText.text = theText
        imageView.image = theImage
        labelDetailDesc.text = theDesc
        self.title = theTitle
    }
}
