//
//  SkillsSegue.swift
//  hakanthorleifssonCVapp
//
//  Created by Håkan Thorleifsson on 2019-11-02.
//  Copyright © 2019 Håkan Thorleifsson. All rights reserved.
//

import UIKit

class SkillsSegue: UIStoryboardSegue {
    
    override func perform() {
        scaleAnimation()
    }

    func scaleAnimation (){
        let toViewController = self.destination
        let fromViewController = self.source
        let containerView = fromViewController.view.superview
        let originalCenter = fromViewController.view.center
        toViewController.view.transform = CGAffineTransform(scaleX: 0.06, y: 0.05)
        toViewController.view.center = originalCenter
        containerView?.addSubview(toViewController.view)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {toViewController.view.transform = CGAffineTransform.identity
            
        }, completion: {succes in
            fromViewController.present(toViewController,animated: false,completion: nil)
        } )
    }
}


